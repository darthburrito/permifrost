# CHANGELOG

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a Changelog](http://keepachangelog.com/).



## Unreleased
---

### New

### Changes

### Fixes

### Breaks


## 0.1.0 - (2020-03-06)
---

### Changes
- [#5](https://gitlab.com/gitlab-data/permifrost/issues/5) Updates to print commands as they run instead of in a batch at the end of a run


## 0.0.2 - (2020-03-04)
---

### Changes
- [#3](https://gitlab.com/gitlab-data/permifrost/issues/3) Removes extra `permissions` cli argument from invocation

### Fixes
- [#4](https://gitlab.com/gitlab-data/permifrost/issues/4) Fix schema revokes referencing databases not in spec


## 0.0.1 - (2020-03-03)
---

### Changes
- Initial release


---
